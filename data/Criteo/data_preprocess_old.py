import os
import numpy
import shutil
import pickle
from collections import Counter
from sys import stdout
import numpy as np
import os
import sys

EACH_FILE_DATA_NUM = 3000000
SIZE_TRAIN_NUM = 500000
SIZE_ADD_ONE_LABEL_NUM = 1000000


def get_train_test_file(file_path, feat_dict_, split_ratio=0.9):
    train_label_fout = open('train_label', 'w')
    train_value_fout = open('train_value', 'w')
    train_idx_fout = open('train_idx', 'w')
    test_label_fout = open('test_label', 'w')
    test_value_fout = open('test_value', 'w')
    test_idx_fout = open('test_idx', 'w')

    COUNT_TRAIN_1 = 0
    COUNT_TRAIN_0 = 0
    COUNT_TEST_1 = 0
    COUNT_TEST_0 = 0

    continuous_range_ = range(1, 14)
    categorical_range_ = range(14, 40)
    cont_min_ = [0, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    cont_max_ = [5775, 257675, 65535, 969, 23159456, 431037, 56311, 6047, 29019, 46, 231, 4008, 7393]
    cont_diff_ = [cont_max_[i] - cont_min_[i] for i in range(len(cont_min_))]
    avg_=[]
    h=[1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000]
    sum_=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    with open(file_path, 'r') as fin:
        for line_idx, line in enumerate(fin):
            features = line.rstrip('\n').split('\t')
            for idx in continuous_range_:
                if features[idx] == '':
                    h[idx-1]=h[idx-1]-1
                else:
                    sum_[idx-1]=sum_[idx-1]+float(features[idx])
    m=0
    for i in sum_:
        avg_.append(i/h[m])
        m=m+1
    def process_line_(line):
        features = line.rstrip('\n').split('\t')
        feat_idx, feat_value, label = [], [], []

        # mean normalization
        for idx in continuous_range_:
            if features[idx] == '':
                feat_idx.append(0)
                feat_value.append(0.0)
            else:
                feat_idx.append(feat_dict_[idx])
                feat_value.append(round((float(features[idx]) - avg_[idx - 1]) / cont_diff_[idx - 1], 6))

        # 处理分类型数据
        for idx in categorical_range_:
            if features[idx] == '' or features[idx] not in feat_dict_:
                feat_idx.append(0)
                feat_value.append(0.0)
            else:
                feat_idx.append(feat_dict_[features[idx]])
                feat_value.append(1.0)
        return feat_idx, feat_value, [int(features[0])]

    with open(file_path, 'r') as fin:
        for line_idx, line in enumerate(fin):
            stdout.write('\rProcessing train test: {:2.2%}'.format(line_idx / 1000000))

            if line_idx >= EACH_FILE_DATA_NUM * 10:
                break

            feat_idx, feat_value, label = process_line_(line)

            feat_value = '\t'.join([str(v) for v in feat_value]) + '\n'
            feat_idx = '\t'.join([str(idx) for idx in feat_idx]) + '\n'
            label = '\t'.join([str(idx) for idx in label]) + '\n'

            if np.random.random() <= split_ratio:
                if int(label[0]) == 1:
                    COUNT_TRAIN_1 += 1
                else:
                    COUNT_TRAIN_0 += 1
                train_label_fout.write(label)
                train_idx_fout.write(feat_idx)
                train_value_fout.write(feat_value)
            else:
                if int(label[0]) == 1:
                    COUNT_TEST_1 += 1
                else:
                    COUNT_TEST_0 += 1
                test_label_fout.write(label)
                test_idx_fout.write(feat_idx)
                test_value_fout.write(feat_value)

        fin.close()

    print('\nQuantity of trian have 1 label:\t' + str(COUNT_TRAIN_1))
    print('Quantity of train have 0 label:\t' + str(COUNT_TRAIN_0))
    print('Qauntity of test have 1 label: \t' + str(COUNT_TEST_1))
    print('Quantity of test have 0 label: \t' + str(COUNT_TEST_0))
    train_label_fout.close()
    train_idx_fout.close()
    train_value_fout.close()
    test_label_fout.close()
    test_idx_fout.close()
    test_value_fout.close()


def get_feat_dict(path_train):
    freq_ = 10
    dir_feat_dict_ = 'feat_dict_' + str(freq_) + '.pkl2'
    continuous_range_ = range(1, 14)
    categorical_range_ = range(14, 40)

    if os.path.exists(dir_feat_dict_):
        feat_dict = pickle.load(open(dir_feat_dict_, 'rb'))
    else:
        # print('generate a feature dict')
        # Count the number of occurrences of discrete features
        feat_cnt = Counter()
        with open(path_train, 'r') as fin:
            for line_idx, line in enumerate(fin):
                stdout.write('\rGet feat dict: {:2.2%}'.format(line_idx / 1000000))
                # for test
                if line_idx >= EACH_FILE_DATA_NUM * 10:
                    break
                features = line.rstrip('\n').split('\t')
                for idx in categorical_range_:
                    if features[idx] == '': continue
                    feat_cnt.update([features[idx]])

        # Only retain discrete features with high frequency
        dis_feat_set = set()
        for feat, ot in feat_cnt.items():
            if ot >= freq_:
                dis_feat_set.add(feat)

        # Create a dictionary for continuous and discrete features
        feat_dict = {}
        tc = 1
        # Continuous features
        for idx in continuous_range_:
            feat_dict[idx] = tc
            tc += 1
        # Discrete features
        cnt_feat_set = set()
        with open(path_train, 'r') as fin:
            for line_idx, line in enumerate(fin):
                # get mini-sample for test
                if line_idx >= EACH_FILE_DATA_NUM * 10:
                    break

                features = line.rstrip('\n').split('\t')
                for idx in categorical_range_:
                    if features[idx] == '' or features[idx] not in dis_feat_set:
                        continue
                    if features[idx] not in cnt_feat_set:
                        cnt_feat_set.add(features[idx])
                        feat_dict[features[idx]] = tc
                        tc += 1

        # Save dictionary
        with open(dir_feat_dict_, 'wb') as fout:
            pickle.dump(feat_dict, fout)
        print('\nargs.num_feat ', len(feat_dict) + 1)

    return feat_dict


def delete_file(paht):
    if os.path.exists(paht):
        os.remove(paht)


if __name__ == '__main__':
    delete_file('feat_dict_10.pkl2')
    delete_file('test_idx')
    delete_file('test_label')
    delete_file('test_value')
    delete_file('train_idx')
    delete_file('train_label')
    delete_file('train_value')
    print(sys.argv)
    if sys.argv[1].split('==')[1] == 'True':
        path_train = 'train_fill_mean.txt'
        print(path_train)
    else:
        path_train = 'train.txt'
        print(path_train)

    feat_dict = get_feat_dict(path_train)
    get_train_test_file(path_train, feat_dict)
    print('Done!')
