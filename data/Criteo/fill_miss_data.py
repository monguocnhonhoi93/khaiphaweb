import pandas as pd
from time import sleep
from sys import stdout
import numpy as np
import pickle

if __name__=='__main__':
    train_txt = open('train_fill_mean.txt', 'w')
    pkl_file = open('fill_mean.pkl', 'rb')
    mean_dict = pickle.load(pkl_file)
    pkl_file.close()
    with open('train.txt', 'r') as fin:
        for line_idx, line in enumerate(fin):
            features = line.rstrip('\n').split('\t')
            for i in range(1, 14):
                name = 'feature_' + str(i)
                if features[i] == '':
                    features[i] = mean_dict[name]
            stdout.write('\rProcess fill miss data: %2.2f' % ((line_idx+1)/10000))
            stdout.write('%')
            for i in range(len(features)):
                train_txt.write(str(features[i]))
                if i < 39:
                    train_txt.write('\t')
            train_txt.write('\n')






