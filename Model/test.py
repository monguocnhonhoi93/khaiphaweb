import pickle


with open('pred_y.pickle', 'rb') as f:
    pred_y = pickle.load(f)

with open('true_y.pickle', 'rb') as f:
    true_y = pickle.load(f)
