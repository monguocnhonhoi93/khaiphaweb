import tensorflow as tf
import math
from sklearn.metrics import roc_auc_score, accuracy_score, precision_score, f1_score, classification_report
import numpy as np
import pickle
from sys import stdout
from time import sleep


BATCH_SIZE = 2048


def train_test_model_demo(model, train_label_path, train_idx_path, train_value_path,
                          test_label_path, test_idx_path, test_value_path, parameter):
    train_batch_dataset = get_batch_dataset(train_label_path, train_idx_path, train_value_path)
    test_batch_dataset = get_batch_dataset(test_label_path, test_idx_path, test_value_path)
    stdout.write(str(train_batch_dataset))
    if parameter['optimizer'] == 'SGD' or parameter['optimizer'] == 'sgd':
        optimizer = tf.keras.optimizers.SGD(learning_rate=float(parameter['learningrate']), momentum=0.9)
    else:
        optimizer = tf.keras.optimizers.Adam(learning_rate=float(parameter['learningrate']))

    print('\nResults of old preprocessor and None class weight are:\n')
    EPOCHS = int(parameter['epochs'])
    for epoch in range(EPOCHS):
        train_model(model, train_batch_dataset, optimizer, epoch, parameter)
        test_model(model, test_batch_dataset, epoch, parameter)


def get_batch_dataset(label_path, idx_path, value_path):
    label = tf.data.TextLineDataset(label_path)
    idx = tf.data.TextLineDataset(idx_path)
    value = tf.data.TextLineDataset(value_path)

    label = label.map(lambda x: tf.strings.to_number(tf.strings.split(x, sep='\t')), num_parallel_calls=12)
    idx = idx.map(lambda x: tf.strings.to_number(tf.strings.split(x, sep='\t')), num_parallel_calls=12)
    value = value.map(lambda x: tf.strings.to_number(tf.strings.split(x, sep='\t')), num_parallel_calls=12)

    batch_dataset = tf.data.Dataset.zip((label, idx, value))
    batch_dataset = batch_dataset.shuffle(buffer_size=20480)
    batch_dataset = batch_dataset.batch(BATCH_SIZE)
    batch_dataset = batch_dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return batch_dataset

# tf.keras.losses.Huber
@tf.function
def cross_entropy_loss(y_true, y_pred, parameter):
    if parameter['is_class_weight'] == 'True':
        zero_weight = float(parameter['class_weight'][0])
        one_weight = float(parameter['class_weight'][1])
        verto = y_true * one_weight + (1 - y_true) * zero_weight
        return tf.reduce_mean(verto * tf.losses.binary_crossentropy(y_true, y_pred))
    else:
        return tf.reduce_mean(tf.losses.binary_crossentropy(y_true, y_pred))


@tf.function
def train_one_step(model, optimizer, idx, value, label, parameter):
    with tf.GradientTape() as tape:
        output = model(idx, value)
        loss = cross_entropy_loss(y_true=label, y_pred=output, parameter=parameter)

        reg_loss = []
        for p in model.trainable_variables:
            reg_loss.append(tf.nn.l2_loss(p))
        reg_loss = tf.reduce_sum(tf.stack(reg_loss))

        loss = loss + model.reg_l2 * reg_loss

    grads = tape.gradient(loss, model.trainable_variables)
    grads = [tf.clip_by_norm(g, 100) for g in grads]
    optimizer.apply_gradients(grads_and_vars=zip(grads, model.trainable_variables))
    return loss


def train_model(model, train_batch_dataset, optimizer, epoch, parameter):
    train_item_count = 41120555
    for batch_idx, (label, idx, value) in enumerate(train_batch_dataset):
        if len(label) == 0:
            break

        loss = train_one_step(model, optimizer, idx, value, label, parameter)

        # if batch_idx % 1000 == 0:
        #     stdout.write('Train Epoch: {} [{} / {} ({:.2f}%)]\tLoss:{:.6f}'.format(
        #         epoch, batch_idx * len(idx), train_item_count,
        #         100. * batch_idx / math.ceil(int(train_item_count / BATCH_SIZE)), loss.numpy()))


def test_model(model, test_batch_dataset, epoch, parameter):
    epochs = int(parameter['epochs'])
    pred_y, true_y = [], []
    binaryloss = tf.keras.metrics.BinaryCrossentropy()
    for batch_idx, (label, idx, value) in enumerate(test_batch_dataset):
        if len(label) == 0:
            break

        output = model(idx, value)
        binaryloss.update_state(y_true=label, y_pred=output)
        pred_y.extend(list(output.numpy()))
        true_y.extend(list(label.numpy()))

    predict = []
    output = open('pred_y.pkl', 'wb')
    pickle.dump(pred_y, output)
    output.close()
    output = open('true_y.pkl', 'wb')
    pickle.dump(true_y, output)
    output.close()
    for i in pred_y:
        if i[0] >= float(parameter['alpha']):
            predict.append(1)
        else:
            predict.append(0)
    label = []
    for i in true_y:
        label.append(i[0])

    if epoch == epochs - 1:
        stdout.write('\rEpoch: %1.0f/%1.0fLoss: %.4f , accuracy_score: %.4f , precision_score: %.4f , f1_score: %.4f' % (
            epoch, epochs,
            binaryloss.result(), accuracy_score(np.array(label), np.array(predict)),
            precision_score(np.array(label), np.array(predict)), f1_score(np.array(label), np.array(predict))))
        print('\n')
        print(classification_report(np.array(label), np.array(predict), target_names=['Không click', 'Có click']))
        for i in parameter:
            print(str(i) + ' : ' + str(parameter[i]))
        stdout.write('Done\n')
    else:
        stdout.write('\rEpoch: %1.0f/%1.0f, Loss: %.4f , accuracy_score: %.4f , precision_score: %.4f , f1_score: %.4f' % (
            epoch+1, epochs,
            binaryloss.result(), accuracy_score(np.array(label), np.array(predict)),
            precision_score(np.array(label), np.array(predict)), f1_score(np.array(label), np.array(predict))))
        stdout.write('\n')
