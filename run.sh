cd data/Criteo
python data_preprocess_old.py --fill_miss==False
cd ../../Model
# shellcheck disable=SC2102
python PNN_TensorFlow.py optimizer==SGD alpha==0.3 is_gen_data==False learningrate==1e-4 fill_miss==0 is_class_weight==False class_weight==[0.1,0.9] epochs==10
echo '*************************************'
python PNN_TensorFlow.py optimizer==Adam alpha==0.3 is_gen_data==False learningrate==1e-4 fill_miss==0 is_class_weight==False class_weight==[0.1,0.9] epochs==10
echo '*************************************'
python PNN_TensorFlow.py optimizer==Adam alpha==0.3 is_gen_data==False learningrate==1e-4 fill_miss==0 is_class_weight==True class_weight==[0.1,0.9] epochs==10
echo '*************************************'
python PNN_TensorFlow.py optimizer==Adam alpha==0.3 is_gen_data==False learningrate==1e-4 fill_miss==0 is_class_weight==True class_weight==[1,1.5] epochs==4
echo '*************************************'
echo '\n'
echo '*************************************'
echo 'Miss Data fill value mean'
echo '*************************************'
echo '\n'
cd ../data/Criteo
python data_preprocess_old.py --fill_miss==True
cd ../../Modle
echo '*************************************'
python PNN_TensorFlow.py optimizer==Adam alpha==0.3 is_gen_data==False learningrate==1e-4 fill_miss==0 is_class_weight==False class_weight==[0.1,0.9] epochs==4
echo '*************************************'
python PNN_TensorFlow.py optimizer==Adam alpha==0.3 is_gen_data==False learningrate==1e-4 fill_miss==0 is_class_weight==True class_weight==[0.1,0.9] epochs==4
echo '*************************************'
python PNN_TensorFlow.py optimizer==Adam alpha==0.3 is_gen_data==False learningrate==1e-4 fill_miss==0 is_class_weight==True class_weight==[1,1.5] epochs==4
echo '*************************************'
